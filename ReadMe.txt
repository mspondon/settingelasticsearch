Installation:

1: INSTALL JAVA (best way is to create a seperate vagrant box and install their)

sudo add-apt-repository ppa:openjdk-r/ppa
sudo apt-get update
sudo apt-get install -y openjdk-8-jdk

2: INSTALL ELASTIC SEARCH

wget https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-1.7.2.tar.gz
tar -zxvf elasticsearch-1.7.2.tar.gz

3: INSTALL ELASTICSEARCH HEAD PLUGIN

elasticsearch-1.7.2/bin/plugin -install mobz/elasticsearch-head/1.7.2

4: CONFIGURE ELASTICSEARCH

goto elasticsearch folder then open config/elasticsearch.yml
Add this two line at end

node.name: "twitter"
cluster.name: mycluster1

5: RUNNING ELASTICSEARCH

goto elasticsearch folder then RUN bin/elasticsearch
Now goto http://localhost:9200/_plugin/head/
If everything seems okay then you will find elasticsearch is running in your browser as localhost

6: RUNNING JAVA PROJECT

In the project folder you will find elasticSearch_fresh_deployment.zip
Unzip the zip file then run the following command

java -classpath config -jar elasticSearch.jar (insert | update | delete | full_text_search | range_search | boolean_search)

for insert , update & delete chanegs you can find on http://localhost:9200/_plugin/head/
Rest all the search result I am printing it to console

MAPPING file is present in the project folder named "mapping.json"

DUE TO LESS AMOUNT OF TIME I AM NOT ABLE TO WRITE TEST CASES , YOU CAN CHANGE CONFIG  FILE AND TEST IT IN VARIOUS CASE
Config file name "elasticSearchIndex.properties" and it is present in config folder under zip file.
