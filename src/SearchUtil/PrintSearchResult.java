package SearchUtil;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import java.util.Map;

public class PrintSearchResult {

    public void printResult(SearchResponse response) {

        SearchHit[] results = response.getHits().getHits();
        for (SearchHit hit : results) {
            Map<String, Object> result = hit.getSource();

            System.out.println(result);
        }
    }
}
