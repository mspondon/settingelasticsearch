import elasticsearch.*;
import model.BaseDataLoad;
import org.elasticsearch.client.Client;
import org.elasticsearch.node.Node;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.elasticsearch.node.NodeBuilder.nodeBuilder;

public class Main {

    private static Node node;
    private static Client client;

    public static void main(String[] args) throws IOException {

        String performOperation = args[0];
        Properties elasticSearchConfig = loadElasticSearchConfigFile("config/elasticSearchIndex.properties");

        openConnection(elasticSearchConfig);

        if (performOperation.equalsIgnoreCase("insert")) {
            BaseDataLoad baseDataLoad = new BaseDataLoad("config/dataConfig.properties");
            CreateIndex createIndex = new CreateIndex(elasticSearchConfig);
            createIndex.insertIntoElasticSearch(baseDataLoad.processJson(), client);
        }
        else if (performOperation.equalsIgnoreCase("update")) {

            UpdateIndex updateIndex = new UpdateIndex(elasticSearchConfig);
            updateIndex.update(client);
        }
        else if (performOperation.equalsIgnoreCase("delete")) {

            DeleteIndex deleteIndex = new DeleteIndex(elasticSearchConfig);
            deleteIndex.delete(client);
        }
        else if (performOperation.equalsIgnoreCase("full_text_search")) {

            FullTextSearch fullTextSearch = new FullTextSearch(elasticSearchConfig);
            fullTextSearch.search(client);
        }
        else if (performOperation.equalsIgnoreCase("range_search")) {

            IntegerSearch integerSearch = new IntegerSearch(elasticSearchConfig);
            integerSearch.searchRange(client);
        }
        else if (performOperation.equalsIgnoreCase("boolean_search")) {

            BooleanSearch booleanSearch = new BooleanSearch(elasticSearchConfig);
            booleanSearch.searchBool(client);
        }
        else {

            System.out.println("Please enter valid input argument");
        }

        closeConnection();

    }

    private static Properties loadElasticSearchConfigFile(String elasticSearchConfig) throws IOException {
        InputStream is = null;
        try {
            is = new FileInputStream(elasticSearchConfig);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Properties properties = new Properties();
        properties.load(is);
        return properties;

    }

    public static void openConnection(Properties properties)
    {
        node = nodeBuilder()
                .clusterName(properties.getProperty("elasticSearchClusterName"))
                .client(true)
                .node();
        client = node.client();
    }

    public static void closeConnection()
    {
        client.close();
        node.stop();
    }
}
