package elasticsearch;

import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.Client;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

public class UpdateIndex {

    private Properties elasticSearchConfig;
    public UpdateIndex(Properties elasticSearchConfig) {

        this.elasticSearchConfig = elasticSearchConfig;

    }

    public void update(Client client) throws IOException {

        String eIndex = elasticSearchConfig.getProperty("elasticSearchIndexName");
        String eType = elasticSearchConfig.getProperty("elasticSearchIndexType");
        String eId = elasticSearchConfig.getProperty("updateIndexID");
        String fieldName = elasticSearchConfig.getProperty("updateIndexFieldName");
        String fieldValue = elasticSearchConfig.getProperty("ipdateIndexFieldValue");

        UpdateRequest updateRequest = new UpdateRequest(eIndex,eType,eId)
                .doc(jsonBuilder()
                        .startObject()
                        .field(fieldName, fieldValue)
                        .endObject());
        try {
            client.update(updateRequest).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
