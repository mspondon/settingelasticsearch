package elasticsearch;

import SearchUtil.PrintSearchResult;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;

import java.util.Properties;

public class BooleanSearch {

    private Properties elasticSearchConfig;

    public BooleanSearch(Properties elasticSearchConfig) {

        this.elasticSearchConfig = elasticSearchConfig;

    }

    public void searchBool(Client client)
    {
        String indexName = elasticSearchConfig.getProperty("elasticSearchIndexName");
        String fieldName = elasticSearchConfig.getProperty("boolFilterFieldName");
        String fieldValue = elasticSearchConfig.getProperty("boolFilterValue");

        SearchResponse response = client.prepareSearch(indexName)
                .setQuery(QueryBuilders.matchAllQuery())
                .setPostFilter(FilterBuilders.boolFilter().must(FilterBuilders.termFilter(fieldName,Boolean.valueOf(fieldValue))))
                .execute()
                .actionGet();

        new PrintSearchResult().printResult(response);

    }
}
