package elasticsearch;

import SearchUtil.PrintSearchResult;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeFilterBuilder;;
import java.util.Properties;

public class IntegerSearch {

    private Properties elasticSearchConfig;

    public IntegerSearch(Properties elasticSearchConfig) {

        this.elasticSearchConfig = elasticSearchConfig;

    }

    public void searchRange(Client client) {

        String[] filterIdentifier = {"lt","gt","lte","gte","eql"};

        String indexName = elasticSearchConfig.getProperty("elasticSearchIndexName");
        String rangeFieldName = elasticSearchConfig.getProperty("rangeFilterFieldName");
        int rangeValue = Integer.parseInt(elasticSearchConfig.getProperty("rangeValue"));

        for(int counter = 0;counter < filterIdentifier.length;counter++) {
            SearchResponse response = client.prepareSearch(indexName)
                    .setQuery(QueryBuilders.matchAllQuery())
                    .setPostFilter(createFilter(rangeFieldName, rangeValue, filterIdentifier[counter]))
                    .execute()
                    .actionGet();
            System.out.println("Range Filtering applied for :" + filterIdentifier[counter]);

            new PrintSearchResult().printResult(response);
        }

    }

    private RangeFilterBuilder createFilter(String fieldName, int rangeValue, String identifier)
    {
        if (identifier == "lt") {
            return FilterBuilders.rangeFilter(fieldName).lt(rangeValue);
        }
        else if (identifier == "gt")
        {
            return FilterBuilders.rangeFilter(fieldName).gt(rangeValue);

        }
        else if (identifier == "lte")
        {
            return FilterBuilders.rangeFilter(fieldName).lte(rangeValue);

        }
        else if (identifier == "gte") {

            return FilterBuilders.rangeFilter(fieldName).gte(rangeValue);
        }
        else
        {
            return FilterBuilders.rangeFilter(fieldName).from(rangeValue).to(rangeValue);

        }
    }

}
