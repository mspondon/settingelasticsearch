package elasticsearch;

import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.client.Client;
import java.io.IOException;
import java.util.Properties;

public class DeleteIndex
{
    private Properties elasticSearchConfig;

    public DeleteIndex(Properties elasticSearchConfig) {

        this.elasticSearchConfig = elasticSearchConfig;

    }

    public void delete(Client client) throws IOException {

        String indexName = elasticSearchConfig.getProperty("elasticSearchIndexName");
        String indexType = elasticSearchConfig.getProperty("elasticSearchIndexType");
        String indexId = elasticSearchConfig.getProperty("deleteIndexID");

        DeleteResponse response = client.prepareDelete(indexName, indexType, indexId)
                .execute()
                .actionGet();

    }
}
