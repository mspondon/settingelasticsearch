package elasticsearch;

import SearchUtil.PrintSearchResult;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;

import java.util.Properties;

public class FullTextSearch {

    private Properties elasticSearchConfig;

    public FullTextSearch(Properties elasticSearchConfig) {

        this.elasticSearchConfig = elasticSearchConfig;

    }

    public void search(Client client) {

        String indexName = elasticSearchConfig.getProperty("elasticSearchIndexName");
        String fieldName = elasticSearchConfig.getProperty("searchIndexFieldName");
        String searchName = elasticSearchConfig.getProperty("searchIndexValue");

        SearchResponse response = client.prepareSearch(indexName)
                .setSearchType(SearchType.QUERY_AND_FETCH)
                .setQuery(QueryBuilders.matchQuery(fieldName, searchName))
                .setFrom(0).setSize(60).setExplain(true)
                .execute()
                .actionGet();

        PrintSearchResult printSearchResult = new PrintSearchResult();
        printSearchResult.printResult(response);
    }

}
