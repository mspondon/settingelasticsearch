package elasticsearch;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import java.util.Properties;

public class CreateIndex {

    private Properties elasticSearchConfig;

    public CreateIndex(Properties configFile) {

        this.elasticSearchConfig = configFile;
    }

    public void insertIntoElasticSearch(String jsonResult, Client client)

    {
        String eIndex = elasticSearchConfig.getProperty("elasticSearchIndexName");
        String eType = elasticSearchConfig.getProperty("elasticSearchIndexType");
        JsonElement jsonElement = new JsonParser().parse(jsonResult);
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        JsonArray jsonArray = jsonObject.getAsJsonArray("hits");
        int counter = 1;
        for (final JsonElement index : jsonArray) {
            JsonObject jsonObject1 = index.getAsJsonObject();
            String source = jsonObject1.get("_source").toString();
            createIndex(eIndex,eType,counter,source,client);
            counter = counter+1;
        }
    }

    private void createIndex(String eIndex, String eType, int eId, String source, Client client) {

        IndexResponse response = client.prepareIndex(eIndex,eType, String.valueOf(eId))
                .setSource(source)
                .execute()
                .actionGet();
    }
}
