package model;

import java.io.*;
import java.util.Properties;

public class BaseDataLoad {

    private Properties dataConfig;

    public BaseDataLoad(String configFile) throws IOException {
        InputStream is = null;
        try {
            is = new FileInputStream(configFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new FileNotFoundException("Configuration file not found");
        }
        dataConfig = new Properties();
        dataConfig.load(is);
    }

    public String processJson() throws IOException {
        String jsonFilePath = dataConfig.getProperty("dataFileName");
        BufferedReader bufferedReader = new BufferedReader(new FileReader(jsonFilePath));
        String currentLine;
        String stringToParse = new String();
        while ((currentLine = bufferedReader.readLine()) != null) {
            stringToParse += String.format("%s%s", currentLine, System.lineSeparator());

        }
        return stringToParse;
    }

}
